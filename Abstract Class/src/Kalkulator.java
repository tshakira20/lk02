abstract class Kalkulator {
    double operan1, operan2;
    //Do your magic here...

    // Mendefinisikan method yang memiliki 2 parameter (operan1 dan operan2)
    public void setOperan(double operan1, double operan2) {
        this.operan1 = operan1;
        this.operan2 = operan2;
    }

    // Mendeklarasikan method hitung yang akan menghitung hasil operasi
    public abstract double hitung();
}