class Pertambahan implements Kalkulator {
    //Do your magic here...

    // Mengimplementasikan method hitung() yang telah dideklarasikan di dalam interface Kalkulator
    public double hitung(double operan1, double operan2) {
        return operan1 + operan2;
    }
}